# Teste de Performance com Operadores Bitwise e Módulo em C

Este repositório contém um código escrito em C para analisar a performance dos operadores bitwise (&) e módulo (%) em estruturas condicionais. O objetivo é comparar a eficiência desses operadores em diferentes cenários.


## Requisitos

Para executar este projeto, você precisa ter:

- Um compilador C instalado em sua máquina. O código foi originalmente compilado com o GCC na versão 14.1.1.
- Git instalado em sua máquina para clonar o repositório.


## Instalação e Execução

Siga os passos abaixo para instalar e executar o projeto:

1. Abra o terminal na pasta onde deseja clonar o projeto e digite o comando:
```bash 
git clone https://gitlab.com/divisplima/bitwise_c.git
```

2. Acesse a pasta do projeto:
```bash
cd bitwise_c
```

3. Compile o programa para a sua máquina. Você pode usar o compilador que preferir. Aqui estão exemplos com `cc` e `clang`:
```bash
cc src/main.c -o bin/main
```
ou
```bash
clang src/main.c -o bin/main
```

4. Execute o programa:
```bash
./bin/main
```

5. Se você encontrar um erro de 'Permissão Negada', use o comando:
```bash
chmod +x ./bin/main
```

## Problemas e Contribuições

Se você encontrar algum problema com o código ou tiver alguma sugestão de melhoria, fico feliz em saber! Aqui estão os passos para reportar um problema ou contribuir:

1. Vá para a página principal do repositório no GitLab.
2. Clique em "Issues" no menu.
3. Clique em "New Issue".
4. Preencha o título e a descrição do problema ou da sugestão. Tente ser o mais claro e detalhado possível.
5. Quando terminar, clique em "Submit issue".

Agradeço antecipadamente por qualquer contribuição que você possa fazer.


## Contato

Sinta-se à vontade para me contatar:

- Nome: Divis Paulino
- E-mail: divisplima@gmail.com
- LinkedIn: https://www.linkedin.com/in/divisplima
  