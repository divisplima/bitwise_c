#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[]) {
	int TEST_CASE_COUNT = 1048576;
    // int TEST_CASE_COUNT = 1000000;
    clock_t start, end;
	double cpu_time_used;
	int n = 0;
    int test_cases[TEST_CASE_COUNT];
    int odd_values;
    int x;
    int temp;

    printf("Variables successfully defined...\r\n");

    /* Generate test cases */
    for(n = 0; n < TEST_CASE_COUNT; n++) 
        test_cases[n] = rand() % (4096);
    printf("Successfully created %d test cases\r\n", TEST_CASE_COUNT);
    printf("\r\n   Initiating benchmark procedures\r\n");

    /* Benchmark of Modulus */
    printf("  :: Benchmarking Division(Modulus) scenarios ::::\r\n");
    odd_values = 0;
	start = clock();
    for(x = 0; x < 1000; x++){
        odd_values = 0;
	    for(n = 0; n < TEST_CASE_COUNT; n++) {
            temp = (test_cases[n] % 2);
            odd_values += temp;
        }
    }

	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("      Execution time: %fs.\r\n      Odd numbers found: %d\r\n", 
        cpu_time_used, 
        odd_values
    );

    /* Benchmark of Bitwise*/
    printf("\r\n  :: Benchmarking Bitwise scenarios ::::");
    odd_values = 0;

	start = clock();
    for(x = 0; x < 1000; x++){
        odd_values = 0;
        for(n = 0; n < TEST_CASE_COUNT; n++) {
            temp = (test_cases[n] & 1);
            odd_values += temp;
        }
    }
	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	
    printf("\r\n      Execution time: %fs.\r\n      Odd numbers found: %d\r\n", 
        cpu_time_used, 
        odd_values
    );
}